// Spam console to check if the extension is loading correctly
console.log("Working!")

// A nice regex filter for jQuery
// See https://j11y.io/javascript/regex-selector-for-jquery/

jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ? 
                        matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels,'')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}

// Hide divs that have a class that contains the text 'promotedlink'
$('div:regex(class,promotedlink)').hide();
